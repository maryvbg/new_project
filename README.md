# Launching App Container in Kubernetes Cluster

The example uses an *ExampleApp* application that has port 8800 to get requests.

1. Create a directory and subdirectories using the commands::

```
mkdir quickstart_docker
mkdir quickstart_docker/application
mkdir quickstart_docker/docker
mkdir quickstart_docker/docker/application
```
where:
- quickstart_docker - Project directory;
- application - Application code directory;
- docker - Configuration for docker;
- application - Directory containing the Dockerfile for the application.

**Note**: use the directory structure given in the example to avoid possible errors.

2. In the ``quickstart_docker/application`` directory, add your application file (for example, *application.py*):

```
import http.server
import socketserver

PORT = 8000

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer(("", PORT), Handler)

print("serving at port", PORT)
httpd.serve_forever()
```

3. In the ``quickstart_docker/docker/docker/application`` directory, add the *Dockerfile* file with the following contents (to configure python and dependencies):

```
# Use base image from the registry
FROM python:3.5

# Set the working directory to /app
WORKDIR /app

# Copy the 'application' directory contents into the container at /app
COPY ./application /app

# Make port 8000 available to the world outside this container
EXPOSE 8000

# Execute 'python /app/application.py' when container launches
CMD ["python", "/app/application.py"]
```

4. To build the image, enter in the terminal:

```
docker build . -f-docker/application/Dockerfile -t exampleapp
```

where:
- ``.`` - working directory, build context;
- ``-f docker/application/Dockerfile`` - docker-file;
- ``-t exampleapp`` - image tag (for easy search).

For more information about building images for Docker, see [link](https://docs.docker.com/engine/reference/builder/).

5. Check the image list:

```
$ docker images
REPOSITORY TAG IMAGE ID CREATED SIZE
exampleapp latest 83wse0edc28a 2 seconds ago 153MB
python 3.6 05sob8636w3f 6 weeks ago 153MB
```

6. Add images to your repository.